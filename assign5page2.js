let arr = [];
let b=document.getElementById('bac')
b.onclick = ()=>{ window.location.href='ass5p1.html'};
let u = new URLSearchParams(window.location.search);
let idnum = u.get('id');
// let use=u.get('name');
let use = localStorage.getItem('name')
let h = document.createElement("h1")
h.style.textAlign = "center"
h.style.color = "#00ADB5"
h.innerHTML = use
document.getElementById("he").appendChild(h);

fetch('https://jsonplaceholder.typicode.com/posts?userId=' + (idnum))
  .then((response) => response.json())
  .then((json) => json.forEach(tableElements))

function tableElements(element) {
  let lab = document.createElement("label")
  lab.className = "labs";
  let checkbox = document.createElement('input');
  checkbox.setAttribute('type', 'checkbox');
  let div = document.createElement("div");
  div.style.background = "linear-gradient( to bottom , rgb(242, 242, 242)10%,rgb(243, 241, 245)50%)"
  div.className = "card"
  // div.style.boxShadow="0 0 1px gray"

  let div1 = document.createElement("div");
  div1.innerHTML += '<div id="post' + element.id + '-head" class="head">' + element.title + '</div><textarea id="ta' + element.id + '" rows="1" cols="80"></textarea><i id="body' + element.id + '-head">' + element.body + '</i><textarea id="taBody' + element.id + '" rows="2" cols="80"></textarea>';
  document.createElement("div")
  div.appendChild(div1)
  div1.id = element.id
  div1.style.paddingBottom = "20px"
  div.style.width = "650px"
  div.style.minHeight = "650px"

  let div2 = document.createElement("div");
  // div2.style.marginTop="10px"
  div2.style.padding = "10px"
  div2.style.textAlign = "initial"
  div2.style.borderTop = "3px dotted gray"
  div.style.borderRadius = "25px"

  div1.childNodes[1].style.display = 'none'
  div1.childNodes[3].style.display = 'none'

  let tra = document.createElement("i")
  tra.className = "fas fa-trash-alt"
  tra.setAttribute('id', 'trash')
  div.innerHTML += '<span class="checkmark"></span>'
  tra.onclick = function () {
    let c = confirm("Are you sure you want to delete post #" + element.id + "?")
    if (c)
      lab.remove()
    else {
      checkbox.checked =  "false"
    }
  }

  div.appendChild(tra)
  let ed = document.createElement("i")
  ed.className = "fas fa-edit"
  ed.setAttribute('id', 'edit')
  div.appendChild(ed)
  ed.onclick = function (e) {
    e.preventDefault()
    let div1txt = div1.childNodes[0]
    let div1txt2 = div1.childNodes[2]

    let q1 = document.getElementById(div1txt.id)
    q1.style.display = 'none'
    let q2 = document.getElementById(div1txt2.id)
    q2.style.display = 'none'

    let txtar = document.getElementById(div1.childNodes[1].id)
    txtar.innerHTML = div1txt.innerHTML
    txtar.style.display = 'block'

    let txtar1 = document.getElementById(div1.childNodes[3].id)
    txtar1.innerHTML = div1txt2.innerHTML
    txtar1.style.display = 'block'
    sav.style.display = 'block'
  }

  let sav = document.createElement("i")
  sav.className = "far fa-save"
  sav.setAttribute('id', 'sav')
  div.appendChild(sav)
  sav.onclick = function (e) {
    console.log(e);
    e.preventDefault();
    let title = document.getElementById('ta' + element.id).value;
    let body = document.getElementById('taBody' + element.id).value;

    document.getElementById('ta' + element.id).style.display = "none";
    document.getElementById('taBody' + element.id).style.display = "none";
    document.getElementById('post' + element.id + '-head').innerHTML = title;
    document.getElementById('post' + element.id + '-head').style.display = "block"
    document.getElementById('body' + element.id + '-head').innerHTML = body;
    document.getElementById('body' + element.id + '-head').style.display = "block"
    sav.style.display = 'none'
  }

  fetch('https://jsonplaceholder.typicode.com/comments?postId=' + (element.id))
    .then((response) => response.json())
    .then((json) => (json.forEach(comments))
    )
  div2.innerHTML += '<span class="comt">' + 'Comments:' + '</span>' + '<br>'
  function comments(element1, i) {
    if (i < 3) {
      div2.innerHTML += '<br>' + '<span class=comment>' + 'Name: </span>' + element1.name + '<br>' + element1.body + '<br>' + '<span class=comment>' + 'Email: ' + '</span>' + element1.email + '<br>'
    }
  }

  checkbox.addEventListener('change', function (e) {
    if (e.target.checked) {
      arr.push(e.target.parentNode)
      if (arr.length > 1) {
        document.getElementById("bod").style.display = 'block'
      }
    }
    else {
      arr.splice(arr.indexOf(this.parentNode), 1)
      if (arr.length <= 1)
        document.getElementById("bod").style.display = 'none'
    }

  });

  div.appendChild(div2)
  document.getElementById("parent").appendChild(lab);
  lab.appendChild(checkbox)
  lab.appendChild(div)
  document.getElementById("bod").onclick = function () {
    let c = confirm("Are you sure you want to delete all the posts selected ?")
    if (c) {
      arr.forEach((e) => {
        // e.style.display="none";
        e.remove();
      })

    }
    else {
      arr.forEach((e)=>{
      e.childNodes[0].checked=false;
      })
      }
    arr = []
    document.getElementById("bod").style.display = 'none'
  }
}
